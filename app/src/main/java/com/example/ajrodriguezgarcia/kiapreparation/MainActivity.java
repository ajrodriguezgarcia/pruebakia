package com.example.ajrodriguezgarcia.kiapreparation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.tenonedesign.t1autograph.T1Autograph;
import com.tenonedesign.t1autograph.T1AutographListener;
import com.tenonedesign.t1autograph.T1CanvasView;
import com.tenonedesign.t1autograph.T1Signature;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import es.gob.afirma.core.AOException;
import es.gob.afirma.signers.pades.InvalidPdfException;
import harmony.java.awt.Color;

import com.clawgrip.signers.pades.bio.PdfBioSigner;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        // Solicitamos el permiso de escritura, como está metido en bruto, puede que la primera vez 'pete' pero solo la primera vez.

        setContentView(R.layout.activity_main);

        final T1CanvasView canvasView = findViewById(R.id.canvas_view);
        final ImageView outputImage = findViewById(R.id.output_image);


        final T1Autograph autograph = new T1Autograph(this); // Creamos el objeto Autograph
        autograph.autographInView(canvasView);
        autograph.setLicenseCode("c32c226070dee1b3ae3461880a91113db154aff0"); // licencia de prueba (deja marca de agua)

        autograph.setListener(new T1AutographListener() {
            @Override
            public void onCompleteWithSignature(T1Autograph t1Autograph, T1Signature t1Signature) {
                Log.d("", "Firma recogida.");
                // png data (Con esto obtenemos la imagen para poder dibujarla en el PDF)
                outputImage.setImageBitmap(BitmapFactory.decodeByteArray(t1Signature.getPngData(), 0, t1Signature.getPngData().length));
                // pdf data (Aquí tenemos el byte[] para poder operar con la firma y dársela al método)
                t1Signature.getPdfData();
                // svg string
                t1Signature.getSvgString();
                // ISO/IEC 19794-7 formatted xml biometric data string
                t1Signature.getXmlString();
                pruebaPDF(t1Signature.getPngData()); // LLAMAMOS AL MÉTODO PRUEBAPDF y le pasamos la imagen
            }

            @Override
            public void onLineEndWithSignaturePointCount(T1Autograph t1Autograph, int i) {
                Log.d("", "Line Ended.");
            }

            @Override
            public void onCancel(T1Autograph t1Autograph) {
                Log.d("", "Cancelled");
            }
        });

        final T1Autograph autographModal = new T1Autograph(this);
        autographModal.setLicenseCode("c32c226070dee1b3ae3461880a91113db154aff0");
        autographModal.setListener(new T1AutographListener() {
            @Override
            public void onCompleteWithSignature(T1Autograph a, T1Signature s) {
                outputImage.setImageBitmap(BitmapFactory.decodeByteArray(s.getPngData(), 0, s.getPngData().length));
            }

            @Override
            public void onLineEndWithSignaturePointCount(T1Autograph a, int i) {
            }

            @Override
            public void onCancel(T1Autograph a) {
            }
        });

             /*
         * Button event handlers
         */
        Button modalButton = findViewById(R.id.modal_button);
        modalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autographModal.showModal("Autorizo a kia a recibir mis datos biometricos para la firma");
            }
        });
        Button doneButton = findViewById(R.id.done_button);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autograph.done();
                autograph.reset();
            }
        });
        Button clearButton = findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autograph.reset();
            }
        });


        Log.d("", "T1Autograph build number: " + autograph.getBuildNumber());

    }

    private void pruebaPDF(final byte[] pdf) {
        File root = new File(Environment.getExternalStorageDirectory().getPath() + "/prueba//"); // Obtenemos la ruta
        root.mkdir(); // Creamos el directorio
        if (!root.exists()) {
            System.out.println("No se ha creado el directorio.");
            // Esto solo salta si no crea el directorio.
        }
        File myFile = new File(Environment.getExternalStorageDirectory().getPath() + "/prueba/prueba.pdf"); // Creamos el fichero

        if (!myFile.exists()) {
            try {
                myFile.createNewFile(); // si no existe, lo forzamos.
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        OutputStream output = null;
        try {
            output = new FileOutputStream(myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Document document = new Document(new Rectangle(36, 600, 144, 760)); // Creamos el documento

        try {
            PdfWriter pdfWriter = PdfWriter.getInstance(document, output);
            document.open();
            document.addCreationDate();
            document.addTitle("DOCUMENTO");
            pdfWriter.setPageEmpty(false);
            document.newPage(); // creamos una nueva página

            ByteArrayOutputStream stream3 = new ByteArrayOutputStream();
            Bitmap bitmap;
            bitmap = (Bitmap) BitmapFactory.decodeByteArray(pdf, 0, pdf.length); // transformamos el byte[] a BitMap
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream3);
            Image image = Image.getInstance(stream3.toByteArray());
            image.scalePercent(2);

            document.add(image); // le añadimos la imagen al documento
            document.close();


            Properties properties = new Properties(); // objeto de properties (vacío)

           ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            FileInputStream fis = new FileInputStream(myFile);

            byte[] data = new byte[(int) myFile.length()]; // Preparamos la transformación a byte[] del documento pdf

            fis.read(data);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                data = Files.readAllBytes(myFile.toPath()); // transformamos el pdf a byte[]
            }

            try (InputStream inStream = new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/CertificatGenerator/RSAAdriandesarrollo 20181127_102126/CA-RSA-cert.crt")) {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                X509Certificate cert = (X509Certificate)cf.generateCertificate(inStream);
                // Cogemos el certificado

                // Guardamos
                byte[] pdfFirmado = PdfBioSigner.bioSignPdf(data,properties,"CN=" + "123123123123", pdf, cert);
                System.out.println(pdfFirmado);
                ByteArrayOutputStream by = new ByteArrayOutputStream();

                File nuevoDoc = new File(Environment.getExternalStorageDirectory().getPath() + "/prueba/pruebad.pdf");
                // Creamos un nuevo File que sobrescriba el prueba.pdf que teníamos antes.
                // Ahora leemos el pdf firmado (que tiene la firma de Biosigner + la página con la imagen del pdf
                // Y lo grabamos
                nuevoDoc.createNewFile();
                FileInputStream prueba1 = new FileInputStream(nuevoDoc);
                prueba1.read(pdfFirmado);
                FileOutputStream pruebaout = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/prueba/pruebad.pdf");
                pruebaout.write(pdfFirmado);


            } catch (CertificateException e) {
                e.printStackTrace();
            }  catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidPdfException e) {
                e.printStackTrace();
            } catch (AOException e) {
                e.printStackTrace();
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
